#ifndef THRESHOLD_H
#define THRESHOLD_H

#include "snaphelper.h"
#include <iostream>
#include "rand.h"
#include <cmath>
#include <vector>
using namespace std;

// Implementation of Bootstrap Percaolation
class ThresholdModel
{
    public:
        typedef Network<int> NetType;

        int threshold;
        double initial_p;
        int net_size;
        NetType net;
        mylib::RandDouble rd;

        ThresholdModel(int m, double p, PUNGraph graph):
            threshold(m),
            initial_p(p),
            net_size(graph->GetNodes()),
            net(graph),
            rd()
        {
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                node_iter.getNode() = 0;
        }

        void seed()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(rd.gen() < initial_p)
                    node_iter.getNode() = 1;
            }

        }

        int getOcupation()
        {
            int base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode();
            return base;
        }

        void oneTimeStep()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(0 == node_iter.getNode())
                {
                    int base(0);
                    for(int i=0; i<node_iter->GetDeg(); ++i)
                    {
                        base += net.getNodeFromId(node_iter->GetNbrNId(i)).getNode();
                    }
                    if(base >= threshold)
                        node_iter.getNode() = 1;
                }
            }
        }

        void sim(bool verbose=false)
        {
            int old_ocupation = getOcupation();
            while(1)
            {
                if(verbose)
                    cout << old_ocupation << endl;
                oneTimeStep();
                int new_ocupation = getOcupation();
                if(new_ocupation == old_ocupation)
                    break;
                else
                    old_ocupation = new_ocupation;
            }
        }
};


class ComplexThresholdModel
{
    public:
        typedef Network<int> NetType;

        double initial_p;
        double _alleethreshold;
        double _migrationrate;
        double threshold;
        int net_size;
        NetType net;
        mylib::RandDouble rd;
        vector<int> snapshot;

        ComplexThresholdModel(double p, double alleethreshold, double migrationrate, PUNGraph graph):
            initial_p(p),
            _alleethreshold(alleethreshold),
            _migrationrate(migrationrate),
            threshold(0),
            net_size(graph->GetNodes()),
            net(graph),
            rd()
        {
            initialize();
            threshold = getThreshold(_migrationrate, _alleethreshold);
        }

        double getThreshold(double mu, double c)
        {
            double sqrtvalue = sqrt(1-c+c*c-3*mu);
            return -(-1-c+sqrtvalue)*(-1-c*c+c*(4+sqrtvalue)+sqrtvalue + 6*mu)/27.;
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                node_iter.getNode() = 0;
                snapshot.push_back(0);
            }
        }

        void seed()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(rd.gen() < initial_p)
                    node_iter.getNode() = 1;
            }
            originToSnap();
        }

        void snapToOrigin()
        {
            for(int i=0; i<net_size; ++i)
            {
                net.getNodeFromId(i).getNode() = snapshot[i];
            }
        }

        void originToSnap()
        {
            for(int i=0; i<net_size; ++i)
            {
                snapshot[i] = net.getNodeFromId(i).getNode();
            }
        }

        int getOcupation()
        {
            int base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode();
            return base;
        }

        bool oneTimeStep()
        {
            bool flag(true);
            // make a copy of the original configuration
            originToSnap();

            // try the dynamics
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(0 == node_iter.getNode())
                {
                    double base(0);
                    // iterate through the neighbors
                    for(int i=0; i<node_iter->GetDeg(); ++i)
                    {
                        NetType::NodeIterator neighbor_iter = net.getNodeFromId(node_iter->GetNbrNId(i));
                        if(neighbor_iter.getNode())
                            base += 1./static_cast<double>(neighbor_iter->GetDeg());
                    }
                    if(base * _migrationrate >= threshold)
                    {
                        snapshot[node_iter->GetId()] = 1;
                        flag = false;
                    }
                }
            }

            // change the configuration
            snapToOrigin();
            return flag;
        }

        void sim(bool verbose=false)
        {
            while(1)
            {
                if(verbose)
                    cout << getOcupation() << endl;
                if(oneTimeStep())
                    break;
            }
        }
};


class LanternPercoaltion
{
    public:
        typedef Network<int> NetType;

        double initial_p;
        int net_size;
        NetType net;
        mylib::RandDouble rd;

        LanternPercoaltion(double p, PUNGraph graph):
            initial_p(p),
            net_size(graph->GetNodes()),
            net(graph),
            rd()
        { 
            initialize();
        }

        void initialize()
        {
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(rd.gen() < initial_p)
                    node_iter.getNode() = 1;
                else
                    node_iter.getNode() = 0;
            }
        }

        int getOcupation()
        {
            int base(0);
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
                base += node_iter.getNode();
            return base;
        }

        void sim()
        {
            std::vector<int> flag;
            for(NetType::NodeIterator node_iter = net.begin(); node_iter != net.end(); ++node_iter)
            {
                if(1 == node_iter.getNode())
                {
                    for(int i=0; i<node_iter->GetDeg(); ++i)
                        flag.push_back(node_iter->GetNbrNId(i));
                }
            }
            for(int index : flag)
            {
                net.getNodeFromId(index).getNode() = 1;
            }
        }

};

#endif
