#ifndef SNAPHELPER_H
#define SNAPHELPER_H

#include <rand.h>
#include <map>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "mymath.h"

#include "Snap.h"


template<typename NodeType>
class Network
{
    public:
        class NodeIterator
        {
            private:
                TUNGraph::TNodeI graph_iter;
                Network& net;
            public:
                NodeIterator(const NodeIterator & other_iter):graph_iter(other_iter.graph_iter), net(other_iter.net) {}
                NodeIterator& operator=(const NodeIterator & other_iter)
                {
                    graph_iter=other_iter.graph_iter;
                    net = other_iter.net;
                    return *this;
                }
                NodeIterator(const TUNGraph::TNodeI & other_graph_iter, Network & other_net):graph_iter(other_graph_iter), net(other_net) {}

                // pre Increment iterator
                NodeIterator& operator++() {graph_iter++; return *this;}
                // post Increment
                NodeIterator operator++(int) {NodeIterator temp = *this; ++*this; return temp;}
                bool operator<(const NodeIterator& other_iter) const {return graph_iter < other_iter.graph_iter;}
                bool operator==(const NodeIterator& other_iter) const {return graph_iter==other_iter.graph_iter;}

                TUNGraph::TNodeI& operator*() {return graph_iter;}
                TUNGraph::TNodeI* operator->() { return &graph_iter; }

                NodeType& getNode() { return net.nodes[graph_iter.GetId()]; }
        };

    private:
        PUNGraph _graph;
        std::vector<NodeType> nodes;
    public:
        int node_number;
        Network(PUNGraph graph): _graph(graph), node_number(graph->GetNodes())
    {
        for(int i=0; i<node_number; ++i)
        {
            nodes.push_back(NodeType());
        }
    }

        NodeIterator begin() { return NodeIterator(_graph->BegNI(), *this); }
        NodeIterator end() { return NodeIterator(_graph->EndNI(), *this); }

        NodeIterator getNodeFromId(const int& id)
        {
            if(id >= node_number)
            {
                return end();
            }
            else
            {
                return NodeIterator(_graph->GetNI(id), *this);
            }
        }
        friend class NodeIterator;
};

// some helper functions
void ExecessDegreeDist(const PUNGraph & graph, std::ostream & out)
{
    int sum(0), degree(0);
    std::map<int, std::vector<int>> store;

    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
    {
        // iterate through the node's neighbors
        degree = nodeiter.GetDeg();
        auto it = store.find(degree);
        if(it == store.end())
        {
            store[degree] = std::vector<int>();
        }
        sum = 0;
        for(int i=0; i<degree; ++i)
        {
            sum += graph->GetNI(nodeiter.GetNbrNId(i)).GetDeg();
        }
        store[degree].push_back(sum / static_cast<double>(degree));
    }
    for(auto & iter: store)
    {
        out << iter.first << " " << mylib::average(iter.second) << std::endl;
    }
}

void degreeDist(const PUNGraph & graph, std::ostream & out)
{
    TVec<TPair<TInt, TInt>> result;
    TSnap::GetOutDegCnt(graph, result);

    for(auto iter = result.BegI(); iter != result.EndI(); iter++)
    {
        out << iter->GetVal1();
        if(iter->GetVal2())
            out << " " << iter->GetVal2() <<std::endl;
        else
            out << " " << 0 << std::endl;
    }
}

void profileNetworks(const PUNGraph & graph, std::string graph_name)
{
    // degree distibution
    std::string filename_base = "./data/";
    std::string file_suffix = ".out";
    std::ostringstream file_name;
    file_name << filename_base << graph_name << "_dist" << file_suffix;
    std::ofstream out;
    out.open(file_name.str());

    degreeDist(graph, out);

    out.close();

    // correlation between degrees
    file_name.str(std::string());
    file_name << filename_base << graph_name << "_exess" << file_suffix;
    out.open(file_name.str());

    ExecessDegreeDist(graph, out);

    out.close();
}

PUNGraph GenGamma(int & nodes, int lambda, double sigma, TRnd& Rnd=TInt::Rnd)
{
    TIntV degree_sequence_vec;
    mylib::RandGammaMeanVarInt rgmvi(lambda, sigma);

    uint degree_sum = 0;
    for(int i=0; i<nodes; ++i)
    {
        int degree = rgmvi.gen();
        degree_sequence_vec.Add(degree);
        degree_sum += degree;
    }
    if(degree_sum % 2 == 1)
        degree_sequence_vec[0] += 1;
    return TSnap::GenConfModel(degree_sequence_vec, Rnd);
//    PUNGraph graph = TSnap::GenDegSeq(degree_sequence_vec, Rnd);
//    return TSnap::GenRewire(graph, 20, Rnd);
}

//double averageDegree(const PUNGraph & graph)
//{
//    TVec<TPair<TInt, TInt>> result;
//    TSnap::GetOutDegCnt(graph, result);
//
//    for(auto iter = result.BegI(); iter != result.EndI(); iter++)
//    {
//        out << iter->GetVal1();
//        if(iter->GetVal2())
//            out << " " << iter->GetVal2() <<std::endl;
//        else
//            out << " " << 0 << std::endl;
//    }
//
//}
//

PUNGraph GenStar(int size)
{
    PUNGraph graph = PUNGraph::New();
    for(int i=0; i<size; ++i)
    {
        graph->AddNode(i);
    }
    for(int i=1; i<size; ++i)
    {
        graph->AddEdge(0, i);
    }
    return graph;
}

PUNGraph GenTrianglarLattice(int grid_length, bool per=true)
{
    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, grid_length, false);
    if(per)
    {
        for(int i=0; i<grid_length; ++i)
        {
    //        cout << i << " " << i + grid_length*(grid_length-1) << endl;
    //        cout << i * grid_length << " " << (i+1) * grid_length - 1 << endl;
            graph->AddEdge(i, i + grid_length*(grid_length-1));
            graph->AddEdge(i * grid_length, (i+1) * grid_length - 1);
        }
    }

    for(int i=0; i<grid_length*grid_length; ++i)
    {
        if(i<grid_length*(grid_length-1) && (i+1)%grid_length!=0)
        {
//            cout << i << " " << i + grid_length + 1 << endl;
            graph->AddEdge(i, i + grid_length + 1);
        }
    }

    if(per)
    {

        for(int i=0; i<grid_length; ++i)
        {
            if(i==0)
            {
                graph->AddEdge(i, grid_length*grid_length-1);
            }
            else
            {
                graph->AddEdge(i, i + grid_length*(grid_length-1) - 1);
                graph->AddEdge(i * grid_length, (i) * grid_length - 1);
            }
        }
    }

    return graph;
}

PUNGraph Gen2dGrid(int L, bool per=true)
{
    PUNGraph graph = TSnap::GenGrid<PUNGraph>(L, L, false);
    if(!per)
        return graph;

    for(int i=0; i<L*L; ++i)
    {
        int x = i % L;
        int y = i / L;

        /* Left */
        if(0 == x)
        {
            graph->AddEdge(i, i+L-1);
        }
        /* Up */
        if(0 == y)
        {
            graph->AddEdge(i, i+L*(L-1));
        }
    }

    return graph;
}

void doumpGraph(PUNGraph graph, std::ostream & out)
{
    for(int i=0; i<graph->GetNodes(); ++i)
    {
        auto node_iter = graph->GetNI(i);
        for(int j=0; j<node_iter.GetDeg(); ++j)
        {
            out << node_iter.GetId() << " " << graph->GetNI(node_iter.GetNbrNId(j)).GetId() << std::endl;
        }
    }
}

double averageShortedPath(const PUNGraph & graph, bool verbose=false)
{
    int n = graph->GetNodes();
    int base(0);

    for(int i=0; i<n; ++i)
    {
        if(verbose)
            std::cout << "Node " << i << "..." << std::endl;
        for(int j=i+1; j<n; ++j)
        {
            base += TSnap::GetShortPath(graph, i, j);
        }
    }
    return base / static_cast<double>(n * (n - 1)/2);
}

int genNetworks()
{
    //    // for parameters
    //    mylib::Parser parser("./config.cfg");
    //    int n = parser.getInt("n");
    //    double p = parser.getDouble("p");

    //    // er random networks
    //    PUNGraph er_graph;
    //    er_graph = TSnap::GenRndGnm<PUNGraph>(n, static_cast<int>(n * (n-1)/2. * p), false);
    //    profileNetworks(er_graph, "er");
    //
    //    // power law networks
    //    PUNGraph power_graph;
    //    power_graph = TSnap::GenRndPowerLaw(n, 2.2);
    //    profileNetworks(power_graph, "powerlaw");
    //
    //    // regular random networks
    //    PUNGraph regular_random;
    //    regular_random = TSnap::GenRndDegK(1000, 4);
    //    profileNetworks(regular_random, "regular_random");
    //
    //    // BA
    //    PUNGraph ba_graph;
    //    ba_graph = TSnap::GenPrefAttach(10000, 4);
    //    profileNetworks(ba_graph, "ba");
    //
    //    // rewrite BA
    //    PUNGraph rewrite_ba = TSnap::GenRewire(ba_graph);
    //    profileNetworks(rewrite_ba, "rewrite_ba");
    return 0;
}

PUNGraph genOneDLattice(int N, bool cycle=false)
{
    PUNGraph graph = TUNGraph::New();
    // add nodes
    for(int i=0; i<N; ++i)
    {
        graph->AddNode(i);
    }
    // add edges
    for(int i=0; i<N; ++i)
    {
        if(i > 0)
            graph->AddEdge(i, i-1);
        if(i<N-1)
            graph->AddEdge(i, i+1);
    }

    if(cycle)
    {
        graph->AddEdge(0, N-1);
    }

    return graph;
}

void attackAbove(PUNGraph graph, int upper_bound)
{
    std::vector<int> node_to_delete;

    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
    {
        if(nodeiter.GetDeg() >= upper_bound)
            node_to_delete.push_back(nodeiter.GetId());
    }

    for(int id : node_to_delete)
        graph->DelNode(id);
}


#endif
