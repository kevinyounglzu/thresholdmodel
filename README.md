# 阈值模型的实现

网络中节点存在 inactive 和 active 两种状态，当 inactive 节点的邻居中出现超过 m 个 active 节点时该节点会百分之百变为 active 。
初始状态下系统中有 p 比例的 active 节点和 1-p 比例的 inactive 节点。

# 扫参数

1. 确定configgen.py里面的参数正确
2. 新建 ./config or 清除 ./config/
3. python configgen.py
4. 确定 Makefile 里面 JOBS 参数的正确，主程序正确
5. 新建存放数据的文件夹 或者清除 ./data/..
6. make run
7. make prundry
8. make prun
