#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "thresholdbase.h"

using namespace std;

int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int grid_length = parser.getInt("l");
    
    int m = parser.getInt("m");
    double p = parser.getDouble("p");

    int repeat = parser.getInt("repeat");

    ofstream out;


    out.open(mylib::tapeFileName("./data/grid/", ".txt", argv[1]));
    out << "# s=" << grid_length << endl;
    out << "# p=" << p << endl;
    out << "# m=" << m << endl;

//    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, grid_length, true);
    PUNGraph graph = Gen2dGrid(grid_length, true);

    for(int i=0; i<repeat; ++i)
    {
//        cout << i << endl;
        ThresholdModel thresh(m, p, graph);
        thresh.seed();
//        cout << thresh.getOcupation() << endl;
        thresh.sim();
        out << thresh.getOcupation() << endl;
    }

    return 0;
}
