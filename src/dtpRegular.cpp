#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "thresholdbase.h"

using namespace std;

int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    double c = parser.getDouble("c");
    double mu = parser.getDouble("mu");
    double p = parser.getDouble("p");

    int repeat = parser.getInt("repeat");

    ofstream out;


    out.open(mylib::tapeFileName("./data/dtpRegular/", ".txt", argv[1]));
    out << "# c=" << c << endl;
    out << "# p=" << p << endl;
    out << "# m=" << mu << endl;
    out << "# k=" << degree << endl;
    out << "# s=" << size << endl;

    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = TSnap::GenRndDegK(size, degree);
        ComplexThresholdModel thresh(p, c, mu, graph);
        thresh.seed();
//        cout << thresh.threshold << endl;
        thresh.sim();
        out << thresh.getOcupation() << endl;
    }

    // test the copy of origin
    
//    int size(1000);
//    int degree(6);
//    PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
//    
//    ComplexThresholdModel thresh(0.5, 0.1, 0.05, graph);
//    thresh.seed();
//    for(int i=0; i<size; ++i)
//    {
//        thresh.snapshot[i] = 1;
//    }
//    thresh.snapToOrigin();
//    for(int i=0; i<size; ++i)
//    {
//        cout << thresh.net.getNodeFromId(i).getNode() << " " << thresh.snapshot[i] << endl;
//    }


//    // test the threshold generator
//    int size(1000);
//    int degree(6);
//    PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
//    
//    ComplexThresholdModel thresh(0.1, 0.1, 0.05, graph);
//
//    auto cs = mylib::linspace(0, 1, 100);
//    for(auto c:cs)
//    {
//        cout << c << " " << thresh.getThreshold(0.05, c) << endl;
//    }

    out.close();

    return 0;
}
