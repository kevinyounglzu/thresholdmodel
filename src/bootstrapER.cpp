#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "thresholdbase.h"

using namespace std;

int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    int m = parser.getInt("m");
    double p = parser.getDouble("p");

    int repeat = parser.getInt("repeat");

    ofstream out;


    out.open(mylib::tapeFileName("./data/bootstrapERk6m3/", ".txt", argv[1]));
    out << "# p=" << p << endl;
    out << "# m=" << m << endl;
    out << "# k=" << degree << endl;
    out << "# s=" << size << endl;

    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
//        cout << i << endl;
        ThresholdModel thresh(m, p, graph);
        thresh.seed();
//        cout << thresh.getOcupation() << endl;
        thresh.sim();
        out << thresh.getOcupation() << endl;
    }

    return 0;
}
