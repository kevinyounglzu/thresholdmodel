# -*- coding: utf8 -*-
import numpy as np


def genRegularRandom():
    sizes = [1000, 2000, 5000, 10000, 20000, 50000]

    #sizes = [1000]
    degree = 4
    repeat = 1000

    ms = [2]
    #cs = [0.1]
    #ps = [0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 0.995]
    ps = np.linspace(0, 0.2, 201)

    counter = 0
    for size in sizes:
        for m in ms:
            for p in ps:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int repeat=%d\n" % repeat)

                    f.write("int m=%d\n" % m)
                    f.write("double p=%f\n" % p)

                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def gen2dGrid():
    lengths = [20, 30, 40, 50]

    repeat = 1000

    ms = [2]
    ps = np.linspace(0, 0.2, 201)

    counter = 0
    for lengths in lengths:
        for m in ms:
            for p in ps:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int l=%d\n" % lengths)
                    f.write("int repeat=%d\n" % repeat)

                    f.write("int m=%d\n" % m)
                    f.write("double p=%f\n" % p)

                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genBootStrapER():
    sizes = [1000, 2000, 5000, 10000]
    degree = 6
    repeat = 1000

    ms = [3]
    #cs = [0.1]
    #ps = [0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 0.995]
    ps = np.linspace(0, 1, 401)

    counter = 0
    for size in sizes:
        for m in ms:
            for p in ps:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int repeat=%d\n" % repeat)

                    f.write("int m=%d\n" % m)
                    f.write("double p=%f\n" % p)

                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genHeteroThreshold():
    size = 1000
    degree = 6
    repeat = 1000

    h = 1.
    times = 4000.

    mus = [0.05]
    cs = [0.1, 0.25, 0.4]
    ps = np.linspace(0., 1., 201)

    counter = 0
    for mu in mus:
        for c in cs:
            for p in ps:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int repeat=%d\n" % repeat)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double p=%f\n" % p)

                    f.write("double h=%f\n" % h)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genHeteroRegular():
    size = 1000
    degree = 4
    repeat = 1000

    h = 1.
    times = 4000.

    mus = [0.05]
    cs = [0.24, 0.34, 0.4]
    ps = np.linspace(0., 1., 201)

    counter = 0
    for mu in mus:
        for c in cs:
            for p in ps:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int repeat=%d\n" % repeat)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double p=%f\n" % p)

                    f.write("double h=%f\n" % h)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genDTPER():
    sizes = [100000, 5000, 1000]
    degree = 6
    repeat = 1000

    h = 1.
    times = 4000.

    mus = [0.05]
    cs = [0.25]
    #cs = [0.19, 0.24, 0.3]
    #cs = np.linspace(0., 0.5, 101)
    ps = np.linspace(0., 0.5, 501)

    counter = 0
    for size in sizes:
        for mu in mus:
            for c in cs:
                for p in ps:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("int repeat=%d\n" % repeat)

                        f.write("double mu=%f\n" % mu)
                        f.write("double c=%f\n" % c)
                        f.write("double p=%f\n" % p)

                        f.write("double h=%f\n" % h)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genDTPRegular():
    sizes = [1000, 2000, 5000]
    degree = 4
    repeat = 1000

    mus = [0.05]
    cs = [0.24]
    ps = np.linspace(0., 0.2, 201)

    counter = 0
    for size in sizes:
        for mu in mus:
            for c in cs:
                for p in ps:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("int repeat=%d\n" % repeat)

                        f.write("double mu=%f\n" % mu)
                        f.write("double c=%f\n" % c)
                        f.write("double p=%f\n" % p)

                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genLanternP():
    sizes = [2000]
    degree = 6
    repeat = 1000
    ps = np.linspace(0., 1., 201)

    counter = 0
    for size in sizes:
        for p in ps:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)
                f.write("double p=%f\n" % p)
                f.write("int repeat=%d\n" % repeat)

                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)

if __name__ == "__main__":
    genDTPER()
